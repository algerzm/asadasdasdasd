﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Text;

namespace SocketHE
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Socket heServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint direccion = new IPEndPoint(IPAddress.Parse("192.168.3.3"), 1222);
            try
            {
                heServer.Bind(direccion);
                heServer.Listen(3);
                Console.WriteLine("Escuchando...");
                Socket Escuchar = heServer.Accept();
                byte[] msg = Encoding.ASCII.GetBytes("{base64:'1234',calidad:90}<EOF>");

                while (true)
                {
                    if (Escuchar.Connected)
                    {
                        Escuchar.Send(msg);
                        break;
                    }
                }

                Escuchar.Shutdown(SocketShutdown.Both);
                Escuchar.Close();
                Console.WriteLine("Conectado con exito");

                heServer.Close();
            }
            catch(Exception error)
            {
                Console.WriteLine("Error: {0}", error.ToString());
            }
            Console.WriteLine("Presione cualquier tecla para continuar");
            Console.ReadLine();
        }
    }
}
